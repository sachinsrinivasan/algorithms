# Author: Sachin Srinivasan

from ordered_set import OrderedSet
import time


class IntervalScheduling:
    def __init__(self):
        self.events = {
            "Adam": {
                "swimming": 0.1,
                "biking": 0.7,
                "running": 0.7
            },
            "Bret": {
                "swimming": 0.3,
                "biking": 0.6,
                "running": 0.2
            },
            "Ricky": {
                "swimming": 0.8,
                "biking": 0.3,
                "running": 0.7
            }
        }
        self.event_type_accessible = {
            "swimming": True,
            "biking": True,
            "running": True
        }

        self.scheduled_events = dict()
        self.total_time_taken = 0
        self.finished_events = {}
        self.ordered_events = OrderedSet(['swimming', 'biking', 'running'])

    def sort_by_finishing_time(self):
        for participant, _time in self.events.items():
            _time['total_time'] = sum(_time.values())
        return dict(sorted(self.events.items(), key=lambda item: item[1]['total_time']))

    @staticmethod
    def filter_participants(events=dict()):
        return events.keys()

    def start_event(self):
        start_time = time.time()
        self.scheduled_events = self.sort_by_finishing_time()
        participants = self.filter_participants(self.scheduled_events)
        for participant in participants:
            for event in self.ordered_events:
                if event in self.scheduled_events[participant]:
                    if self.start_swimming(participant):
                        break
        end_time = time.time() - start_time
        print("Total Execution Time: ", end_time, " seconds")
        print("Time Spent In All The Events: ", self.total_time_taken, " seconds")
        print("Total Wait Time: ", end_time - self.total_time_taken, "seconds")

    def start_scheduling_events(self, event_type, time_taken):
        self.hold_and_release_events(event=event_type, value=False)
        self.set_timer(time_taken)
        self.calculate_total_time_taken(time_taken)
        self.hold_and_release_events(event=event_type, value=True)

    def start_swimming(self, participant):
        try:
            self.start_scheduling_events("swimming", self.scheduled_events[participant]['swimming'])
            return True
        finally:
            self.start_biking(participant)

    def start_biking(self, participant):
        self.start_scheduling_events("biking", self.scheduled_events[participant]['biking'])
        self.start_running(participant)
        return True

    def start_running(self, participant):
        self.start_scheduling_events("running", self.scheduled_events[participant]['running'])
        return True

    def hold_and_release_events(self, event, value):
        self.event_type_accessible[event] = value
        return True

    def calculate_total_time_taken(self, time_taken):
        self.total_time_taken += time_taken
        return True

    @staticmethod
    def set_timer(timer_count):
        time.sleep(timer_count)
        return True


interval_scheduling = IntervalScheduling()
IntervalScheduling().start_event()
